# rails db:drop && rails db:migrate && rails db:seed

# NOTE: Data generation
# video_id = 'ThMNECpBjUg'
# deck = Deck.where(video_id: video_id).first
# entries = deck.entries.map{|e| e.attributes.slice('begin', 'end', 'text1', 'text2') }
# Rails.root.join("test/fixtures/files/#{video_id}.json").write(JSON.pretty_generate(entries))

user = User.create!(username: 'person1', password: 'asdfjkl;')

d1 = user.decks.create(name: 'Настя из Германии', video_id: 'mWPMDuIG47I', practice_mode: true)
d2 = user.decks.create(name: 'Артём, Настя, и Тамара', video_id: 'ThMNECpBjUg', practice_mode: true)

d1.entries.create!(JSON.parse(Rails.root.join("db/seeds_data/mWPMDuIG47I.json").read))
d2.entries.create!(JSON.parse(Rails.root.join("db/seeds_data/ThMNECpBjUg.json").read))
