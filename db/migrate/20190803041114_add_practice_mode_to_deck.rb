class AddPracticeModeToDeck < ActiveRecord::Migration[5.2]
  def change
    add_column :decks, :practice_mode, :boolean
  end
end
