class AddUsernameToUser < ActiveRecord::Migration[5.2]
  # NOTE: It is irreversible since email becomes blank for "down" direction
  def change
    add_column :users, :username, :string, null: false, default: ''
    reversible do |dir|
      dir.up do
        User.all.each do |user|
          user.update!(username: user.email)
        end
      end
    end
    add_index :users, :username, unique: true

    remove_index :users, :email
    remove_index :users, :reset_password_token
    remove_column :users, :email
    remove_column :users, :reset_password_token
    remove_column :users, :reset_password_sent_at
    remove_column :users, :remember_created_at
    remove_column :users, :admin
  end
end
