class CreateEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :entries do |t|
      t.references :deck, foreign_key: true
      t.text :text1
      t.text :text2
      t.string :begin
      t.string :end

      t.timestamps
    end
  end
end
