Rails.application.routes.draw do
  # TODO: remove routes /users/cancel and /users/edit
  devise_for :users

  devise_scope :user do
    namespace :admin do
      resources :users
      resources :decks
      resources :entries
      resources :rates
    end

    controller 'home' do
      get 'practice'
      post 'quick_preview'
      post 'rate'
      patch 'remote_update'
      # TODO: Change to patch
      get 'generate_api_key'
      get 'export'

    end

    root 'home#index'
  end
end
