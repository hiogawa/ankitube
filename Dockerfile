FROM ruby:2.4.1

# For rails runtime
RUN apt-get update && apt-get install -y npm

# version >= 2 is required
ENV BUNDLER_VERSION='2.0.2'
RUN gem install bundler --no-document -v '2.0.2'

# Save time by letting this layer cache compiled native extension (cf scripts.sh native_gems)
RUN gem install \
bcrypt:3.1.13 \
bindex:0.8.1 \
bootsnap:1.4.4 \
byebug:11.0.1 \
childprocess:1.0.1 \
concurrent-ruby:1.1.5 \
ffi:1.11.1 \
msgpack:1.3.0 \
nio4r:2.4.0 \
nokogiri:1.10.3 \
pg:1.1.4 \
puma:3.12.1 \
rb-fsevent:0.10.3 \
thread_safe:0.3.6 \
unf_ext:0.0.7.6 \
websocket-driver:0.7.1

WORKDIR /app

# Make these layer cachable without whole source which changes always (i.e. COPY . /app)
COPY package.json /app/
RUN npm install
COPY Gemfile Gemfile.lock /app/
RUN bundle install --without development,test

COPY . /app
RUN rails assets:precompile

CMD bundle exec rails s -b 0.0.0.0
