require "administrate/base_dashboard"

class DeckDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    video_id: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    practice_mode: Field::Boolean,
    # TODO: Implement json handling as Field
    subtitle_info_json: Field::String,
    subtitle1_url: Field::String,
    subtitle2_url: Field::String,
    entries: Field::HasMany.with_options(limit: 20),
    # TODO: Inherit Field::HasMany and do cool stuff
    entries_attributes: ::DeckEntriesField,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :video_id,
    :entries,
    :practice_mode,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :id,
    :name,
    :video_id,
    :practice_mode,
    :created_at,
    :updated_at,
    :entries,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :name,
    :video_id,
    :practice_mode,
    :subtitle_info_json,
    :subtitle1_url,
    :subtitle2_url,
    :entries_attributes,
  ].freeze

  # Overwrite this method to customize how decks are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(deck)
    deck.name.truncate(20)
  end
end
