require "administrate/base_dashboard"

class EntryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    deck: Field::BelongsTo,
    id: Field::Number,
    text1: Field::Text,
    text2: Field::Text,
    begin: Field::String,
    end: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    rates: Field::HasMany,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :text1,
    :text2,
    :rates,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :text1,
    :text2,
    :begin,
    :end,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :deck,
    :text1,
    :text2,
    :begin,
    :end,
  ].freeze

  # Overwrite this method to customize how entries are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(entry)
    "Entry ##{entry.id}"
  end
end
