class Rate < ApplicationRecord
  belongs_to :entry
  has_one :deck, through: :entry
  has_one :user, through: :deck

  # value's semantics
  # 0 => ignore
  # 1 => easy
  # 2 => good
  # 3 => difficult
end
