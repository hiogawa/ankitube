require 'rest-client'
require 'rexml/document'

class Deck < ApplicationRecord
  belongs_to :user
  has_many :entries, dependent: :destroy
  accepts_nested_attributes_for :entries
  # TODO: should validate entry exists at least one?

  def next_entry(entry)
    es = self.ordered_entries
    es[(es.index(entry) + 1) % es.length]
  end

  def ordered_entries
    self.entries.to_a.sort_by(&:begin_sec)
  end

  def get_stats
    h = self.entries.includes(:rates).to_a
          .group_by{ |e| e.rates.last&.value }
          .map{ |k, a| [k, a.length] }.to_h
    {0=>0, 1=>0, 2=>0, 3=>0, nil=>0}.merge(h)
  end

  def get_entry_for_practice
    es = self.entries.includes(:rates)
    es = es.reject{ |e| e.rates.to_a.find{ |r| r.value == 0 } } # Ignore rate 0
    es1, es2 = es.partition{|e| e.rates.length == 0 }
    if es1.length > 0
      return es1.shuffle[0]
    else
      return es2.shuffle.sort_by{ |e| e.rates.last.value }.last
    end
  end

  # Used only temporarily during deck creation
  attr_accessor :subtitle_info, :subtitle_info_json, :subtitle1_url, :subtitle2_url
  after_initialize { |record| record.subtitle_info ||= { tracks: [], translations: [] } }

  def subtitle_info_json
    self.subtitle_info.to_json
  end

  def subtitle_info_json=(value)
    self.subtitle_info = JSON.parse(value).with_indifferent_access
  end

  def download_subtitle_list
    self.subtitle_info = Deck.get_subtitle_info(self.video_id)
    self.name = "#{self.subtitle_info[:author]}: #{self.subtitle_info[:title]}"
  rescue => err
    logger.error(err.message)
    self.subtitle_info = { tracks: [], translations: [] }
    self.errors.add(:base, 'Faild to obtain video information')
  end

  def download_entries
    es1 = Deck.get_entries(self.subtitle1_url)
    es2 = Deck.get_entries(self.subtitle2_url)
    self.entries.new(Deck.merge_entries(es1, es2))
  rescue => err
    logger.error(err.message)
    self.errors.add(:base, 'Faild to obtain subtitle texts')
  end

  def self.parse_video_string(value)
    video_id = nil
    if value.length == 11
      video_id = value
    elsif value.match(/youtube\.com|youtu\.be/)
      url = URI.parse(value)
      if url.host == 'youtu.be'
        video_id = url.path[1, url.path.length]
      else
        video_id = url.query.match(/v=(.{11})/)[1]
      end
    end
    video_id
  end

  def self.new_for_quick_preview(video_id_or_url, lang1, lang2)
    video_id = Deck.parse_video_string(video_id_or_url)
    subtitle_info = Deck.get_subtitle_info(video_id)
    if sub1 = subtitle_info[:tracks].find{ |tk| tk[:vssId] == ".#{lang1}" }
      subtitle1_url = sub1[:url]
      if sub2 = subtitle_info[:tracks].find{ |tk| tk[:vssId] == ".#{lang2}" }
        subtitle2_url = sub2[:url]
      else
        subtitle2_url = "#{sub1[:url]}&tlang=#{lang2}"
      end
    elsif sub1 = subtitle_info[:tracks].find{ |tk| tk[:vssId] == "a.#{lang1}" }
      subtitle1_url = sub1[:url]
      subtitle2_url = "#{sub1[:url]}&tlang=#{lang2}"
    end
    es1 = Deck.get_entries(subtitle1_url)
    es2 = Deck.get_entries(subtitle2_url)
    Deck.new(
      video_id: video_id,
      name: "#{subtitle_info[:author]}: #{subtitle_info[:title]}",
      practice_mode: false,
      entries_attributes: Deck.merge_entries(es1, es2),
    )
  end

  def self.get_subtitle_info(video_id)
    resp = RestClient.get("https://www.youtube.com/watch?v=#{video_id}",
        headers={ 'Accept-Language': 'en-US,en' })
    metadata = JSON.parse(resp.body.match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/)[1])
    player_response = JSON.parse(metadata.dig('args', 'player_response'))
    tracks =       player_response.dig('captions', 'playerCaptionsTracklistRenderer', 'captionTracks')
    translations = player_response.dig('captions', 'playerCaptionsTracklistRenderer', 'translationLanguages')
    {
      author: player_response.dig('videoDetails', 'author'),
      title:  player_response.dig('videoDetails', 'title'),
      tracks: tracks.map{|t| {
        vssId: t.dig('vssId'),
        name: t.dig('name', 'simpleText'),
        url: "#{t.dig('baseUrl')}&fmt=ttml" } },
      translations: translations.map{|t| {
        name: t.dig('languageName', 'simpleText'),
        code: t.dig('languageCode') } }
    }
  end

  def self.get_entries(url)
    resp = RestClient.get(url)
    doc = ::REXML::Document.new(resp.body)
    doc.root.get_elements('/tt/body/div/p').map do |p|
      {
        begin: p.attribute('begin').value,
        end: p.attribute('end').value,
        text: p.children.select{|c| c.class == ::REXML::Text }.map(&:value).join(' ')
      }
    end
  end

  def self.merge_entries(entries1, entries2)
    entries1.map do |e1|
      {
        begin: e1[:begin],
        end:   e1[:end],
        text1: e1[:text],
        text2: (entries2.find{|e2| e1[:begin] == e2[:begin]} || {})[:text]
      }
    end
  end
end
