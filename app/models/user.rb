class User < ApplicationRecord
  devise :database_authenticatable, :registerable
  has_many :decks, dependent: :destroy
  has_many :entries, through: :decks
  has_many :rates, -> { order(:created_at) }, through: :entries
  before_create :generate_api_key

  def generate_api_key
    self.api_key = SecureRandom.base58(24)
  end
end
