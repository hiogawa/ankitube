class Entry < ApplicationRecord
  belongs_to :deck
  has_one :user, through: :deck
  has_many :rates, dependent: :destroy

  def begin_slim
    Entry.second_to_timestamp(self.begin_sec)
  end

  def end_slim
    Entry.second_to_timestamp(self.end_sec)
  end

  def begin_sec
    Entry.timestamp_to_second(self.begin)
  end

  def end_sec
    Entry.timestamp_to_second(self.end)
  end

  def self.timestamp_to_second(t)
    h, m, s = t.split(':').map(&:to_i)
    ((h * 60) + m) * 60 + s
  end

  def self.second_to_timestamp(s)
    s = s.to_i
    s = s >= 0 ? s : 0
    m, s = s.divmod(60)
    h, m = m.divmod(60)
    _, h = h.divmod(100)
    sprintf("%02d:%02d:%02d", h, m, s)
  end
end
