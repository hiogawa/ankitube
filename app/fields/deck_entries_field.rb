require "administrate/field/base"

class DeckEntriesField < Administrate::Field::Base
  def self.permitted_attribute(attr, _options = nil)
    { entries_attributes: [:begin, :end, :text1, :text2] }
  end
end
