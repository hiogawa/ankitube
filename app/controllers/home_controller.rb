require 'csv'

class HomeController < ApplicationController
  before_action :authenticate_user!, except: [:export]

  def index
    @decks = current_user.decks.order(created_at: :desc)
    t = Time.now.at_end_of_day
    @rates_by_dates = 7.times.map do |i|
      [
        (t - i.day).to_date,
        (0..3).map{ |i| [i, 0] }.to_h.merge(
          current_user.rates.where(created_at: (t - (i + 1).day)..(t - i.day))
          .to_a.group_by(&:value).transform_values(&:length)
        )
      ]
    end
  end

  def quick_preview
    video_id_or_url = params[:quick_review].require(:video_id_or_url)
    # TODO: Implement default language setting UI
    deck = Deck.new_for_quick_preview(video_id_or_url, 'ru', 'en')
    deck.user = current_user
    if deck.save
      redirect_to practice_path(deck_id: deck.id)
    else
      redirect_to root_path, alert: 'Quick preview failed.'
    end
  end

  def practice
    if params[:deck_id] && params[:entry_id]
      @deck = current_user.decks.find(params[:deck_id])
      @rate_entry = @deck.entries.find(params[:entry_id])
      @seq_mode = params[:seq_mode]
    else
      decks = current_user.decks.where(practice_mode: true)
      if decks.length == 0
        return redirect_to(root_path, alert: 'Please select decks to practice.')
      else
        @deck = decks.sample
        @rate_entry = @deck.get_entry_for_practice
      end
    end
    @practice_info = {
      video_id: @deck.video_id,
      begin_sec: @rate_entry ? @rate_entry.begin_sec : 0,
      # TODO: when "autoplay: false, loop: true", it does autoplay video.
      autoplay: current_user.autoplay,
      loop: current_user.loop,
    }
  end

  def rate
    entry = current_user.entries.find(params[:rate][:entry_id])
    if entry.rates.create(value: params[:rate][:value])
      if params[:seq_mode]
        deck = entry.deck
        next_entry = deck.next_entry(entry)
        redirect_to practice_path(deck_id: deck.id, entry_id: next_entry.id, seq_mode: params[:seq_mode])
      else
        redirect_to practice_path
      end
    else
      head :bad_request
    end
  end

  def remote_update
    klass = Object.const_get(params.require(:model).camelcase)
    attrs = params.require(klass.model_name.singular).permit(:id, :loop, :autoplay, :practice_mode)
    @model = klass.find(attrs[:id])
    authorize @model, :update?
    @model.update(attrs)
    respond_to :js
  end

  def generate_api_key
    current_user.generate_api_key
    current_user.save!
    redirect_to :root
  end

  def export
    if user = User.where(id: params[:id], api_key: params[:api_key]).first
      case params[:type]
      when 'decks'
        output = CSV.generate(col_sep: "\t") do |csv|
          user.decks.each do |deck|
            csv << [deck.id, deck.name, deck.video_id]
          end
        end
        render plain: output
      when 'rates'
        output = CSV.generate(col_sep: "\t") do |csv|
          user.rates.includes(:deck).each do |rate|
            csv << [rate.entry.deck.id, rate.entry.id, rate.value, rate.created_at]
          end
        end
        render plain: output
      else
        head :bad_request
      end
    else
      head :unauthorized
    end
  end
end
