module Admin
  class DecksController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = Deck.
    #     page(params[:page]).
    #     per(10)
    # end

    def new
      deck = Deck.new(user: current_user)
      render locals: {
        page: Administrate::Page::Form.new(dashboard, deck),
      }
    end

    def create
      deck = Deck.new(resource_params)
      deck.practice_mode = true
      if params[:button] == 'create' && deck.save
        redirect_to(
          [namespace, deck],
          notice: translate_with_resource("create.success"),
        )
      else
        if params[:button] == 'load_subtitle_list'
          deck.download_subtitle_list
        elsif params[:button] == 'load_subtitle_texts'
          deck.download_entries
        end
        render :new, locals: {
          page: Administrate::Page::Form.new(dashboard, deck),
        }
      end
    end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   Deck.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
