class EntryPolicy < ApplicationPolicy
  def show?
    user.id == record.user.id
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    user.id == record.user.id
  end

  def edit?
    update?
  end

  def destroy?
    update?
  end

  class Scope < Scope
    def resolve
      user.entries
    end
  end
end
