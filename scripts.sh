#!/bin/bash

case $1 in
  start)
    docker-compose up -d db_dev
    trap "docker-compose stop db_dev" EXIT
    rails s
  ;;
  deploy)
    docker-compose build builder &&\
    docker push registry.heroku.com/ankitube/web &&\
    heroku container:release web
  ;;
  start_ytdl)
    docker-compose up -d ytdl_dev
    trap "docker-compose stop ytdl_dev" EXIT
    exec docker-compose exec ytdl_dev bundle exec rerun -b ruby app.rb
  ;;
  deploy_ytdl)
    docker-compose build ytdl &&\
    docker push registry.heroku.com/youtube-dl-service/web &&\
    heroku container:release --app youtube-dl-service web
  ;;
  database_url)
    heroku config -s | ruby -ne 'puts $_.match(/DATABASE_URL=.(.*)./)[1] if $_.match(/DATABASE_URL/)'
  ;;
  native_gems)
    bundle show --paths 2>/dev/null | ruby -ne 'puts $_.chomp.split("/").last.gsub(/(\-)(\d+)((\.\d+)*)/, ":\\2\\3 \\") if File.directory?("#{$_.chomp}/ext")'
  ;;
  production_rails)
    shift
    spring stop
    export RAILS_ENV=production
    export DATABASE_URL=$(bash scripts.sh database_url)
    exec rails $@
  ;;
  *)
    echo ":: No command is executed."
    exit 1
  ;;
esac
