namespace :misc do
  desc "Fix duplicated entries"
  task fix_duplicate_entries: :environment do
    Deck.find(7).ordered_entries.group_by(&:text1).values.each do |e1, e2|
      puts "#{e1.rates.length} - #{e1.text1}"
      e1.destroy
    end
  end
end
