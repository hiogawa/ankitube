require 'csv'

# NOTE: This was used when migrate data from my old spreadsheet-based learning system
namespace :import do
  # Usage: rake import:csv['lib/tasks/Anki - PracticeLog.csv']
  desc "Import old csv data"
  task :csv, [:file_name] => :environment do |t, args|
    path = Pathname.new(args.file_name)
    raise 'Provide single file path as an argument' unless path.file?
    rows = CSV.read(path)
    header = rows.shift

    rate_conversion = {
      'W' => 0,
      'A' => 1,
      'B' => 2,
      'C' => 3,
    }
    deck_index_conversion = {
      '1' => 'ThMNECpBjUg', # Артём, Настя, Тамара
      '2' => 'MWqkY02Rm-I', # Поглука по Петербруг
      '3' => 'T8mRUFn4oxU', # Полиглоты
      '4' => 'mWPMDuIG47I', # Настя из Германии
      '5' => 'MAD8E-SybXU', # Артём, Настя
      '6' => '2Ytz1QhVNGI', # Прощения
      '7' => 'qA8m2ORwmls', # Фактов о Маша
    }
    rows.each do |row|
      time, rate, deck_index, sentence_index = row
      video_id = deck_index_conversion[deck_index]
      next unless video_id
      deck = Deck.find_by!(video_id: video_id)
      entry = deck.entries[sentence_index.to_i - 1]
      m = time.match(/(\d+)\/(\d+)\/(\d+) (\d+)\:(\d+)\:(\d+)/)            # 7/29/2019 12:00:00 (in csv (JST assumed))
      created_at = "#{m[3]}-#{m[1]}-#{m[2]}T#{m[4]}:#{m[5]}:#{m[6]}+09:00" # 2019-07-29T12:00:00+09:00 (rfc3339)
      entry.rates.create!(created_at: created_at, value: rate_conversion[rate])
      puts "#{rate}: #{entry.text1}"
    end
  end
end
