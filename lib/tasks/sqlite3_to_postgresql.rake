# NOTE: This was used when migrate data from sqlite3 to postgresql
namespace :sqlite3_to_postgresql do
  desc "Export json"
  task :export => :environment do
    data = [User, Deck, Entry, Rate].map do |klass|
      [ klass.model_name.plural, klass.all.map(&:attributes) ]
    end.to_h
    File.write('lib/tasks/data.json', JSON.pretty_generate(data))
  end

  desc "Import json"
  task :import => :environment do
    data = JSON.parse(File.read('lib/tasks/data.json'))
    User.create!({
      "id": 1,
      "email": "foo@example.com",
      "password": "asdfjkl;"
    })
    [Deck, Entry, Rate].each do |klass|
      data[klass.model_name.plural].each do |attrs|
        klass.create!(attrs)
      end
    end
  end

  # This should be run after :import
  # cf. https://stackoverflow.com/questions/11068800/rails-auto-assigning-id-that-already-exists
  desc "Reset primary key numbering"
  task :reset_pk => :environment do
    [User, Deck, Entry, Rate].each do |klass|
      ActiveRecord::Base.connection.reset_pk_sequence!(klass.table_name)
    end
  end
end
