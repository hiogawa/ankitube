require 'test_helper'

class DeckTest < ActiveSupport::TestCase
  test 'create' do
    assert create(:deck, user: create(:user)).valid?
  end

  test 'new_for_quick_preview' do
    deck = Deck.new_for_quick_preview('mWPMDuIG47I', 'ru', 'en')
    assert deck.entries.present?
  end

  test 'parse_video_string' do
    assert Deck.parse_video_string('https://www.youtube.com/watch?v=bVlFUcVNErs') == 'bVlFUcVNErs'
    assert Deck.parse_video_string('https://youtu.be/KcEUEHgKPqc') == 'KcEUEHgKPqc'
  end
end
