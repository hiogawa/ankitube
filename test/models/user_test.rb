require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'create' do
    assert create(:user).valid?
  end

  test 'user_with_decks' do
    assert create(:user_with_decks).valid?
  end
end
