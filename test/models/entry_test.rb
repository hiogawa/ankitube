require 'test_helper'

class EntryTest < ActiveSupport::TestCase
  test 'create' do
    assert create(:entry, deck: create(:deck, user: create(:user))).valid?
  end

  test 'timestamp_to_second' do
    assert Entry.timestamp_to_second('03:47:08.373') == ((3 * 60) + 47) * 60 + 8
  end

  test 'second_to_timestamp' do
    assert Entry.second_to_timestamp(((3 * 60) + 47) * 60 + 8) == '03:47:08'
  end
end
