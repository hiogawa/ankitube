require 'test_helper'

class RateTest < ActiveSupport::TestCase
  test 'create' do
    assert create(:rate, entry: create(:entry, deck: create(:deck, user: create(:user)))).valid?
  end
end
