require 'test_helper'

class HomeControllerTest < ApplicationIntegrationTest
  test 'index failure' do
    get root_url
    assert_response :redirect
  end

  test 'index success' do
    sign_in create(:user)
    get root_url
    assert_response :success
  end

  test 'practice failure' do
    sign_in create(:user)
    get practice_url
    assert_response :redirect
  end

  test 'practice success' do
    sign_in create(:user_with_decks)
    get practice_url
    assert_response :success
  end

  test 'rate failure' do
    skip
    # sign_in create(:user_with_decks)
    # post rate_url
    # assert_response :redirect
  end

  test 'rate success' do
    user = create(:user_with_decks)
    entry = user.entries.first
    sign_in user
    post rate_url, params: { rate: { entry_id: entry.id, value: 2 } }
    assert_redirected_to practice_url
  end

  test 'update failure' do
    skip
    # patch rate_url
  end

  test 'update success 1' do
    user = create(:user)
    sign_in user
    patch update_url, params: { type: 'user', user: { loop: 1, autoplay: 0 } }
    assert_redirected_to root_url
  end

  test 'update success 2' do
    skip
    # patch update_url
  end

  test 'generate_api_key success' do
    sign_in create(:user)
    get generate_api_key_url
    assert_redirected_to root_url
  end

  test 'export success' do
    user = create(:user, api_key: 'asdfjkl;asdfjkl;')
    get export_url, params: { id: user.id, api_key: user.api_key, type: 'rates' }
    assert_response :success
  end
end
