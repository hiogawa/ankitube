FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "person#{n}" }
    password  { 'abcdefgh' }

    factory :user_with_decks do
      after(:create) do |user|
        create_list(:deck_with_entries, 2, user: user)
      end
    end
  end

  factory :deck do
    sequence(:name) { |n| "Deck #{n}" }
    video_id  { |n| 'NayFvKFZNAA' } # random hour-long video
    practice_mode { true }

    factory :deck_with_entries do
      after(:create) do |deck|
        create_list(:entry_with_rates, 10, deck: deck)
      end
    end
  end

  factory :entry do
    sequence(:begin) { |n| Entry.second_to_timestamp(n*5 % (60**2)) }
    sequence(:end)   { |n| Entry.second_to_timestamp((n+1)*5 % (60**2)) }
    sequence(:text1) { |n| "foofoo#{n}" }
    sequence(:text2) { |n| "heehee#{n}" }

    factory :entry_with_rates do
      after(:create) do |entry|
        create_list(:rate, 2, entry: entry)
      end
    end
  end

  factory :rate do
    sequence(:value) { [0, 1, 2, 3].sample }
  end
end
