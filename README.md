AnkiTube

https://ankitube.herokuapp.com/

```
# Development
$ npm install
$ bundle install
$ bash scripts.sh start

# Deployment
$ bash scripts.sh deploy

# Production configuration
$ heroku config:set RAILS_ENV=production RAILS_LOG_TO_STDOUT=yes \
                    RAILS_SERVE_STATIC_FILES=yes RAILS_MASTER_KEY=$(cat config/master.key)

# Production database migration
$ heroku run bash
> rails db:migrate
# or directly connect it from local machine and code
$ bash scripts.sh production_rails db:migrate

# Interact with production database
#   e.g. setting up initial data (here data.json file is provided from your local machine)
#     - Create data.json file from local development db by:
$ rails sqlite3_to_postgresql:export
#     - Then create production database record from data.json by:
$ bash scripts.sh production_rails sqlite3_to_postgresql:import

# Heroku one-time setup
$ heroku create
$ heroku rename ankitube
$ heroku addons:create heroku-postgresql
```

TODO

- Show practice log within practice page
- Backup production database
- Create data for guest user
- Data validation (database level as well) (e.g. uniqueness, requirement, format)
- Persist video_info under Deck
- Implement 'admin/decks#edit'
- Employ administrate custom field to implemment user_decks and deck_entries

References

- https://api.rubyonrails.org/
- https://github.com/plataformatec/devise
- https://github.com/thoughtbot/administrate
- https://github.com/thoughtbot/factory_bot
- https://gitlab.com/hiogawa/heroku_docker_study
