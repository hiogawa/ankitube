```
# Development
$ bash scripts.sh start_ytdl
# or
$ docker-compose exec ytdl_dev bash
> ... do some ..

# Heroku setup
$ heroku create
$ heroku apps:rename --app obscure-chamber-70253 youtube-dl-service

# Deployment
$ bash scripts.sh deploy_ytdl
```
