require 'sinatra'
require 'pry' if development?

set :bind, '0.0.0.0'
set :port, ENV['PORT']

def parse(value)
  video_id = nil
  if value.length == 11
    video_id = value
  elsif value.match(/youtube\.com|youtu\.be/)
    url = URI.parse(value)
    if url.host == 'youtu.be'
      video_id = url.path[1, url.path.length]
    else
      video_id = url.query.match(/v=(.{11})/)[1]
    end
  end
  video_id
end

get '/' do
  video_id = parse(params[:video].to_s)
  erb <<-INDEX
<form action="/download" method="get">
  <input placeholder="Video ID or URL" type="text" name="video" value="#{video_id}">
  <button name="button" type="submit">Download</button>
</form>
INDEX
end

get '/download' do
  headers \
    'Access-Control-Allow-Origin' => '*',
    'Access-Control-Request-Method' => 'GET',
    'Access-Control-Request-Headers' => ''

  video_id = parse(params[:video].to_s)
  return redirect '/' unless video_id
  command = "youtube-dl \
  https://www.youtube.com/watch?v=#{video_id} \
  --extract-audio \
  --add-metadata \
  --audio-format m4a \
  -o '#{Dir.tmpdir}/%(title)s-%(id)s.%(ext)s'"
  rd, wr = IO.pipe
  spawn(command, :out=>wr)
  wr.close
  output = []
  logger.info('====== STARTING youtube-dl ======')
  rd.each_line{ |l| output.push(l); logger.info(l.strip) }
  logger.info('====== FINISHED youtube-dl ======')
  filename = output.join.match(/\[ffmpeg\] Adding metadata to \'(.*)\'$/)[1]
  send_file filename, disposition: :attachment
end
